import os
import logging

from datetime import datetime
from flask import Flask, request, send_file
from logging.handlers import TimedRotatingFileHandler
from simple_salesforce import exceptions as SFexceptions

from user.jobs import Jobs
from system.util import fetch_parameter


logging.basicConfig(level=logging.DEBUG,
                    handlers=[TimedRotatingFileHandler("logs//VeevaOTASync.log", when="midnight"),
                              logging.StreamHandler()],
                    format='%(asctime)s - %(module)s.%(funcName)s() - %(levelname)s - %(message)s')
logging.getLogger('requests').setLevel(logging.ERROR)
logging.getLogger('paramiko').setLevel(logging.ERROR)
logging.getLogger('urllib3').setLevel(logging.ERROR)

app = Flask(__name__)
PREFIX = '/veevaot2a'
METHODS = ["GET", "POST"]


@app.route(PREFIX + '/veevaot2adelta', methods=METHODS)
def OTA_Veeva_Sync():
    def remove_sc_from_queue(job_id):
        # import pdb;pdb.set_trace()
        with open(os.path.join('system', 'job_id.txt') , 'r+') as f:
            a = f.read()
            a = a.replace(job_id, '')
            f.truncate(0)
            f.seek(0)
            f.write(a)
            logging.info(f"job_id {job_id} has been removed from 'In Progress'")

    logging.debug("\n\n .................................. Beginning OTA Job  ..................................\n")
    # logging.debug(request.form)
    sf_username = fetch_parameter(request, "sf_username", default='', type=str)  # source org username
    sf_password = fetch_parameter(request, "sf_password", default='', type=str)  # source org password
    sf_token = fetch_parameter(request, "sf_token", default='', type=str)  # source org securitytoken
    sf_sandbox = fetch_parameter(request, "sf_sandbox", default="False", type=str)  # source org sandbox (bool)
    namespace = fetch_parameter(request, "namespace", default='', type=str)  # is source org managed
    sf_sandbox = True if sf_sandbox.lower() == "true" else False
    veeva_sf_username = fetch_parameter(request, "veeva_sf_username", default='', type=str)  # source org username
    veeva_sf_password = fetch_parameter(request, "veeva_sf_password", default='', type=str)  # source org password
    veeva_sf_token = ''#fetch_parameter(request, "veeva_sf_token", default='', type=str)  # source org securitytoken
    veeva_sf_sandbox = fetch_parameter(request, "veeva_sf_sandbox", default="False", type=str)  # source org sandbox (bool)
    veeva_sf_sandbox = True if veeva_sf_sandbox.lower() == "true" else False
    logger_id = fetch_parameter(request, 'logger_id', default='', type=str)
    logger_object_name = fetch_parameter(request, 'logger_object_name', default='', type=str)
    org_name = fetch_parameter(request, 'org_name', default='', type=str)
    job_id = 'JobID_'+ datetime.now().strftime("%Y-%m-%d-%H-%M-%S")



    try:
        folder = 'ota' + '_' + datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
        tmp_dir = os.path.join('tmp', folder)
        os.mkdir(tmp_dir)
        # os.chmod(tmp_dir, mode=0777)
        job = Jobs(sf_username, sf_password, sf_token, sf_sandbox, namespace, request.host, tmp_dir, veeva_sf_username, veeva_sf_password, veeva_sf_token, veeva_sf_sandbox, logger_id, logger_object_name,org_name)
        response = job.invoke()

    except SFexceptions.SalesforceAuthenticationFailed as auth:
        logging.critical(auth)
        remove_sc_from_queue(job_id)
        return str(auth)

    except Exception as e:
        logging.exception("ERROR")
        remove_sc_from_queue(job_id)
        return str(e)

    remove_sc_from_queue(job_id)
    return response

    if not response:
        remove_sc_from_queue(job_id)
        return 'Completed Successfully'
    if response == 'Validation Failed':

        remove_sc_from_queue(job_id)
        return "Validation Failed"
    remove_sc_from_queue(job_id)
    return f"Error Occurred \n {str(response)}"



@app.route(PREFIX + '/download', methods=METHODS)
def download():
    # return os.path.abspath(os.path.join('tmp',fname))
    # return os.path.abspath(os.path.join('tmp',fname))
    path = fetch_parameter(request, 'filepath', default='', type=str)
    file_name = path.split("/")[-1]
    attachment = fetch_parameter(request, 'attachment_filename', default=file_name, type=str)

    # sandbox         = fetch_parameter(request, 'sandbox', default='False', type=str)
    return send_file(os.path.abspath(path), mimetype='application/zip',
                     as_attachment=True, attachment_filename=attachment)


@app.before_request
def log_request_info():
    logging.info('Request ::: %r' % request.url)
    job_id = 'JobID_'+ datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    if job_id:
        with open(os.path.join('system', 'job_id.txt'), 'r+') as f:
            a = f.read()
            if a.find(job_id) != -1:
                output = f"Duplicate Hit! Scenario {job_id} already in progress"
                logging.critical(output)
                return output
            else:
                f.truncate(0)
                f.seek(0)
                f.write(a + job_id)
            logging.info("job_id %s has been logged as 'In Progress'" % job_id)


@app.errorhandler(500)
def internal_server_error(error):
    logging.error('Server Error: %s', (error))
    return f"Unhandled Server Error:  {str(error)}"


if __name__ == '__main__':
    app.run(debug=True)



