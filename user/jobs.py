import os
import glob
import json
import logging
import paramiko
import traceback
import pandas as pd
import datacompy as dcp
from salesforce_bulk import SalesforceBulk
from datetime import datetime, timezone, timedelta
import time
from system.salesforce import SalesforceConnector
from system.postgres import Postgres
import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from email import encoders
import unicodecsv as csv
import base64
from email.mime.base import MIMEBase
import mimetypes
from zipfile import ZipFile

upload_folder = "Ready_to_Upload"


class Jobs():
	def __init__(self, sf_username: str, sf_password: str, sf_security_token: str, sf_sandbox: bool, namespace: str, ip_addr,
				 tmp_dir, veeva_sf_username: str, veeva_sf_password: str, veeva_sf_security_token: str,
				 veeva_sf_sandbox: bool, logger_id: str, logger_object_name: str, org_name: str):

		self.namespace = namespace
		self.response = True
		self.ip_addr = ip_addr
		self.tmp_dir = tmp_dir
		self.logger_id = logger_id
		self.org_name= org_name
		self.logger_object_name = logger_object_name
		self.sf_salesiq = SalesforceConnector(sf_username, sf_password, sf_security_token, namespace, sf_sandbox,
											  tmp_dir)
		self.sf_veeva = SalesforceConnector(veeva_sf_username, veeva_sf_password, veeva_sf_security_token, '', veeva_sf_sandbox, tmp_dir)
		self.bulk = SalesforceBulk(username=sf_username, password=sf_password, security_token=sf_security_token,
								   sandbox=sf_sandbox)
		self.pg_main = Postgres.initialise_from(self.sf_salesiq)
		self.pg_ob = Postgres.initialise_from_2(self.sf_salesiq)
		self.data_object_start_datetime = datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4] + "Z"

	def invoke(self):
		try:
			logging.info('\n\n ........... OTA Generation invoked. Beginning services !!! .............\n')
			#self.veeva_ota_fullsync()
			os.mkdir(os.path.join(self.tmp_dir, upload_folder))
			last_run_date, data_object_id = self.last_success_run_date(self.logger_id, self.logger_object_name)
			logging.debug(last_run_date)
			all_pa_table, live_team_instance = self.fetch_ti_details()
			no_load_country = self.get_no_load_country_list()
			self.run_copy_position_account_function(all_pa_table, live_team_instance)
			self.veeva_master_sync(last_run_date, data_object_id)
			salesiq_pa = self.get_data_from_pg_master()
			veeva_ota = self.get_data_from_pg_ota(no_load_country)
			join_columns = 'accountnumber, position_code'
			object_name = 'ObjectTerritory2Association'
			ota_delete, ota_insert = self.compare_data(veeva_ota, salesiq_pa, join_columns, object_name)
			insert_df = self.prepare_insert_file(ota_insert)
			response, bulk_success_filepath, error_df = self.sf_veeva.bulk_v2_functions('insert', object_name, insert_df)
			file_path_insert_error = os.path.abspath(os.path.join('tmp', "ota_inset_error" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv'))
			error_df.to_csv(file_path_insert_error, index = False)
			count_insert_success = self.update_success_records_dbota(bulk_success_filepath)
			count_insert_error = self.insert_error_records_db(error_df)
			delete_df = self.prepare_delete_file(ota_delete)
			file_path_1 = os.path.join('tmp', "ota_temp_del_df" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv')
			delete_df.to_csv(file_path_1, index = False)        
			response_del, bulk_success_deleted_filepath, error_df_1 = self.sf_veeva.bulk_v2_functions('delete',object_name, delete_df)
			count_delete_success = self.deleted_records_dbota(bulk_success_deleted_filepath)
			count_delete_error = self.delete_error_records_db(error_df_1)
			self.sucees_error_mail(count_insert_success, count_delete_success, count_insert_error, count_delete_error,file_path_insert_error)
			self.mark_logger_success()
			return ''

		except Exception as e:
			self.mark_logger_error(e)
			logging.exception("ERROR")
			#self.mark_error()
			return str(e)

	def fetch_ti_details(self):
		job = self.bulk.create_query_job("AxtriaSalesIQTM__Data_Set_Rule_Map__c", contentType='CSV')
		batch = self.bulk.query(job, "select AxtriaSalesIQTM__table_name__c, "
									 "AxtriaSalesIQTM__dataset_id__r.AxtriaSalesIQTM__Data_Set_Object_Name__c, "
									 "AxtriaSalesIQTM__dataset_id__r.AxtriaSalesIQTM__Scenario__r"
									 ".AxtriaSalesIQTM__Team_Instance__c, "
									 "AxtriaSalesIQTM__dataset_id__r.AxtriaSalesIQTM__Scenario__c from "
									 "AxtriaSalesIQTM__Data_Set_Rule_Map__c where "
									 "AxtriaSalesIQTM__dataset_id__r.AxtriaSalesIQTM__Scenario__r"
									 ".AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = "
									 "'Current'  and "
									 "AxtriaSalesIQTM__dataset_id__r.AxtriaSalesIQTM__Scenario__r"
									 ".AxtriaSalesIQTM__Scenario_Stage__c = 'Live' and "
									 "AxtriaSalesIQTM__dataset_id__r.AxtriaSalesIQTM__Scenario__r"
									 ".AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r"
									 ".AxtriaSalesIQTM__Country__r.AxtriaARSnT__Veeva_Push__c = true and "
									 "AxtriaSalesIQTM__dataset_id__r.AxtriaSalesIQTM__Data_Set_Object_Name__c in ("
									 "'Account','AxtriaSalesIQTM__Position_Account__c')")
		self.bulk.close_job(job)
		while not self.bulk.is_batch_done(batch):
			time.sleep(10)
		all_accounts_table = []
		all_pos_account_table = []
		all_team_instances = []
		all_scenarios_ids = []
		counter = 0
		for result in self.bulk.get_all_results_for_query_batch(batch):
			for row in result:
				if counter == 0:
					counter = counter + 1
					continue
				temp_str = row.decode().replace('"', '').split(',')
				t_fin_do_table = 't_fin_do'
				if temp_str[1].split('\n')[0] == 'AxtriaSalesIQTM__Position_Account__c':
					if temp_str[0] in all_pos_account_table:
						counter = 1
					elif t_fin_do_table in temp_str[0]:
						all_pos_account_table.append(temp_str[0])
				else:
					if temp_str[0] in all_accounts_table:
						counter = 1
					else:
						all_accounts_table.append(temp_str[0])
				all_team_instances.append(temp_str[2].split('\n')[0])
				all_scenarios_ids.append(temp_str[3].split('\n')[0])
		logging.info('Following are Tables of Position Account Fetched from Salesforce')
		logging.info(all_pos_account_table)
		logging.info('Fetching Tables from Master Sync Object, Copying Data to Temp Table - masterTableATL')
		return all_pos_account_table, all_team_instances

	def run_copy_position_account_function(self, tablesnames, team_instance_ids):
		temptable = 'z_team_instance'
		self.pg_main.execute_query("CREATE TABLE IF NOT EXISTS z_team_instance (scenario_id text)")
		self.pg_main.execute_query("TRUNCATE z_team_instance")
		team_instance_ids_str = ', '.join(team_instance_ids)
		self.pg_main.execute_query("INSERT INTO z_team_instance(scenario_id) VALUES ('" + team_instance_ids_str + "')")
		self.pg_main.execute_query("CREATE TABLE IF NOT EXISTS z_master_pa (accountnumber text, position_code text, "
								   "status text)")
		self.pg_main.execute_function('posgresFunctionForMasterSyncCopy', tablesnames, temptable)
		logging.info('Data Successfully fetched from Master Sync Table')

	def veeva_ota_fullsync(self):
		try:
			table_name = 'veeva_ota'
			query_account = 'SELECT Id, ObjectId, Territory2Id, LastModifiedDate FROM ObjectTerritory2Association'
			#query_territory2 = 'SELECT Id, Name FROM Territory2 WHERE LastModifiedDate >= {}'.format(last_run_date)
			veeva_ota_full_sync = self.sf_veeva.bulk_download('ObjectTerritory2Association', query_account,
															   'veeva_ota_' + datetime.now().strftime(
																   "%Y-%m-%d %H-%M-%S"))

			self.pg_ob.execute_query("CREATE TABLE IF NOT EXISTS veeva_ota (id text, objectid "
									 "text, territory2id text,lastmodifieddate timestamp)")
			self.pg_ob.execute_query("TRUNCATE TABLE {}".format(table_name))
			self.pg_ob.execute_query("COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(table_name, veeva_ota_full_sync))
			return 'success'
		except Exception as e:
			logging.exception("ERROR")
			#self.mark_error()
			return str(e)

			

	def veeva_master_sync(self, last_run_date, data_object_id):
		try:
			query_account = 'SELECT Id,External_ID_vod__c, Status_AZ__c,Country_Code_AZ__c FROM Account where ' \
							'createddate >= {}'.format(last_run_date)
			query_territory2 = 'SELECT Id, Name FROM Territory2 where createddate >= {}'.format(last_run_date)
			veeva_delta_accounts = self.sf_veeva.bulk_download('Account', query_account,
															   'veeva_account_' + datetime.now().strftime(
																   "%Y-%m-%d %H-%M-%S"))

			self.pg_ob.execute_query("CREATE TABLE IF NOT EXISTS veeva_account (account_id text, external_id_vod "
									 "text, status_az text,country_vod text)")
			veeva_account_table = 'veeva_account'
			#if not veeva_delta_accounts:
			df_account = pd.read_csv(veeva_delta_accounts, dtype='object', encoding='utf-8')
			if not df_account.empty:
				self.update_data_postgres(veeva_account_table, df_account)
			veeva_territory2_table = 'veeva_territory2'
			self.pg_ob.execute_query("CREATE TABLE IF NOT EXISTS veeva_territory2 (territory2_id text, name text)")
			# veeva_delta_territory2 = self.sf_veeva.bulk_download('Territory2', query_territory2, 'territory2_' + datetime.now().strftime("%Y-%m-%d %H-%M-%S"))
			# #if not veeva_delta_territory2:
			# df_terr2 = pd.read_csv(veeva_delta_territory2, dtype='object', encoding='utf-8')
			# if not df_terr2.empty:
			# 	self.update_data_postgres(veeva_territory2_table, df_terr2)
			query_account_gas = 'SELECT Id, ObjectId, Territory2Id, LastModifiedDate FROM ObjectTerritory2Association ' \
			'WHERE ObjectId IN (SELECT Account__c FROM GAS_Alignment_History_vts__c where createddate >= {} ) and lastmodifieddate >= {} '.format(last_run_date,last_run_date)
			logging.debug(query_account_gas)
			veeva_gas = self.sf_veeva.bulk_download('ObjectTerritory2Association', query_account_gas, 'gas_ota_' + datetime.now().strftime("%Y-%m-%d %H-%M-%S"))
			self.pg_ob.execute_query("COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format('veeva_ota', veeva_gas))
			querydel = """DELETE FROM veeva_ota a USING(SELECT MIN(ctid)as ctid , CONCAT(objectid, territory2id) as key FROM veeva_ota GROUP By key HAVING count(*) > 1 ) b WHERE CONCAT(a.objectid, a.territory2id) = b.key AND a.ctid <> b.ctid ;"""
			self.pg_ob.execute_query(querydel)
			self.mark_success(self.data_object_start_datetime, data_object_id)
			return 'success'
		except Exception as e:
			logging.exception("ERROR")
			#self.mark_error()
			return str(e)

	def mark_logger_success(self):
		logging.debug("\n\n Marking logger object as success")
		self.sf_salesiq.update(self.logger_object_name,self.logger_id,"Status__c","Success")

	def mark_logger_error(self, e):
		self.sf_salesiq.compound_update(self.logger_object_name, self.logger_id, {
					"Status__c": "Error",
					"Stack_Trace__c": str(e)})

	def update_data_postgres(self, table_name, df):
		if table_name == 'veeva_account':
			df.rename(columns={'Id': 'account_id', 'External_ID_vod__c': 'external_id_vod', 'Status_AZ__c': 'status_az',
							   'Country_Code_AZ__c': 'country_vod'}, inplace=True)
			filepath = os.path.abspath(
				os.path.join('tmp', "veeva_account_" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv'))
			df.to_csv(filepath, index=False)
			del df
			temptable = table_name + '_temp'
			self.pg_ob.execute_query("CREATE TABLE IF NOT EXISTS {} (account_id text, external_id_vod "
									 "text, status_az text,country_vod text)".format(temptable))
			self.pg_ob.execute_query("COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(temptable, filepath))
			self.pg_ob.execute_query(
				"DELETE FROM {} WHERE account_id IN (SELECT account_id FROM {});".format(table_name, temptable))
			self.pg_ob.execute_query(
				"COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(table_name, filepath))
			self.pg_ob.execute_query("TRUNCATE TABLE {}".format(temptable))
		else:
			df.rename(columns={'Id': 'territory2_id', 'Name': 'name'}, inplace=True)
			filepath = os.path.abspath(
				os.path.join('tmp', "territory2_" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv'))
			df.to_csv(filepath, index=False)
			del df
			temptable = table_name + '_temp'
			self.pg_ob.execute_query("CREATE TABLE IF NOT EXISTS {} (territory2_id text, name text)".format(temptable))
			self.pg_ob.execute_query("COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(temptable, filepath))
			self.pg_ob.execute_query(
				"DELETE FROM {} WHERE territory2_id IN (SELECT territory2_id FROM {});".format(table_name, temptable))
			self.pg_ob.execute_query(
				"COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(table_name, filepath))
			self.pg_ob.execute_query("TRUNCATE TABLE {}".format(temptable))
		return 'Success'

	def last_success_run_date(self, record_id, object_name):
		object_id = self.sf_salesiq.query(
			"SELECT Id, AxtriaSalesIQTM__Last_Success_Sync_End_Date__c FROM AxtriaSalesIQTM__Data_Object__c WHERE Name = 'VeevaOT2A'", 'AxtriaSalesIQTM__Data_Object__c')
		if not object_id.empty:
			last_run_date = object_id.at[0, 'AxtriaSalesIQTM__Last_Success_Sync_End_Date__c']
			last_run_date = last_run_date.replace('+0000', 'Z')
			data_object_id = object_id.at[0,'Id']
			return last_run_date, data_object_id
		else:
			last_run_date = '2020-01-01'
			return last_run_date

	def get_no_load_country_list(self):
		no_load_countries = self.sf_salesiq.query(
			"SELECT AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c WHERE AxtriaARSnT__Veeva_Push__c = False", 'AxtriaSalesIQTM__Country__c' )
		logging.debug("No lOad Country List ---- :"+no_load_countries)
		return no_load_countries

	def mark_error(self, e, data_object_id):
		logging.error("Marking error in Data_Object__c and Logger object {}".format(self.logger_object_name))
		try:
			self.sf_salesiq.compound_update('Data_Object__c', data_object_id,
											{'Status__c': 'Error', 'Sync_Response_Detail__c': str(e)[:1999]})
		except Exception as e:
			logging.exception("Error")

	def mark_success(self, current_dtime, data_object_id):
		self.sf_salesiq.compound_update('Data_Object__c', data_object_id, {
			'Status__c': 'Success',
			'Last_Success_Sync_Start_Date__c': current_dtime,
			'Last_Success_Sync_End_Date__c': self.current_datetime()
		})

	def get_data_from_pg_master(self):
		query = "SELECT accountnumber, position_code FROM z_master_pa where status IN ('Active', 'Future Active')"
		columns = """accountnumber, position_code"""
		return self.pg_main.download_data(query, self.tmp_dir, 'MasterPASalesIQ', columns)

	def get_data_from_pg_ota(self, no_load):
		if not no_load.empty:
			noload = no_load['AxtriaSalesIQTM__Country_Code__c'].to_list()
		else:
			empty_noload= []
			noload = empty_noload
		query = "SELECT objectid, territory2id FROM veeva_ota "
		columns = """objectid, territory2id"""
		df_ota = self.pg_ob.download_data(query, self.tmp_dir, 'VeevaOT2A', columns)
		query_v_account = "SELECT account_id, external_id_vod, country_vod FROM veeva_account WHERE status_az = 'Active'"
		vacolumns = """account_id, external_id_vod, country_vod"""
		veeva_accounts = self.pg_ob.download_data(query_v_account, self.tmp_dir, 'VeevaAcc', vacolumns)
		query_v_terr2 = "SELECT territory2_id, name FROM veeva_territory2"
		vtcolumns = """territory2_id, name """
		veeva_terr2 = self.pg_ob.download_data(query_v_terr2, self.tmp_dir, 'VeevaTerr2', vtcolumns)
		df_ota = pd.merge(df_ota, veeva_accounts, left_on=['objectid'], right_on=['account_id'])
		df_ota = pd.merge(df_ota, veeva_terr2, left_on=['territory2id'], right_on=['territory2_id'])
		df_ota = df_ota.loc[~df_ota['country_vod'].isin(noload)]
		df_ota = df_ota.loc[:, ['external_id_vod', 'name']]
		df_ota.rename(columns={'external_id_vod': 'accountnumber', 'name': 'position_code'}, inplace=True)
		return df_ota

	def compare_data(self, df1, df2, join_columns, object_name):
		to_delete = os.path.abspath(
			os.path.join('tmp', object_name + "_delete_" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv'))
		to_insert = os.path.abspath(
			os.path.join('tmp', object_name + "insert_" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv'))
		compare = dcp.Compare(df1, df2, join_columns=['accountnumber', 'position_code'], abs_tol=0, rel_tol=0, df1_name='original',
							  df2_name='new', filepath1=to_delete, filepath2=to_insert)
		#logging.debug("Compare Results" + compare)
		df_delete = pd.read_csv(to_delete, dtype='object', encoding='utf-8')
		df_insert = pd.read_csv(to_insert, dtype='object', encoding='utf-8')
		return df_delete, df_insert

	def current_datetime(self):
		return datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4] + "Z"

	def set_column_sequence(self, dataframe, seq, front=True):
		cols = seq[:]
		for x in dataframe.columns:
			if x not in cols:
				if front:
					cols.append(x)
				else:
					cols.insert(0, x)
		return dataframe[cols]

	def prepare_insert_file(self, ota_insert):
		query_v_account = "SELECT account_id, external_id_vod FROM veeva_account WHERE status_az = 'Active'"
		vacolumns = """account_id, external_id_vod"""
		veeva_accounts = self.pg_ob.download_data(query_v_account, self.tmp_dir, 'VeevaAcc', vacolumns)
		query_v_terr2 = "SELECT territory2_id, name FROM veeva_territory2"
		vtcolumns = """territory2_id, name"""
		veeva_terr2 = self.pg_ob.download_data(query_v_terr2, self.tmp_dir, 'VeevaTerr2', vtcolumns)
		df_new = pd.merge(ota_insert, veeva_accounts, left_on=['accountnumber'], right_on=['external_id_vod'])
		df_new = pd.merge(df_new, veeva_terr2, left_on=['position_code'], right_on=['name'])
		df_new.rename(columns={'territory2_id': 'Territory2Id', 'account_id': 'ObjectId'}, inplace=True)
		df_new = df_new[['Territory2Id','ObjectId']]
		df_new['AssociationCause'] = 'Territory2Manual'
		df_new = df_new.dropna()
		logging.debug('----Insert File Colimns----'+df_new.columns)
		return df_new

	def prepare_delete_file(self, ota_delete):
		query_v_account = "SELECT account_id, external_id_vod FROM veeva_account WHERE status_az = 'Active'"
		vacolumns = """account_id, external_id_vod"""
		veeva_accounts = self.pg_ob.download_data(query_v_account, self.tmp_dir, 'VeevaAcc', vacolumns)
		query_v_terr2 = "SELECT territory2_id, name FROM veeva_territory2"
		vtcolumns = """territory2_id, name"""
		veeva_terr2 = self.pg_ob.download_data(query_v_terr2, self.tmp_dir, 'VeevaTerr2', vtcolumns)
		query_v_ota = "SELECT id, territory2id, objectid FROM veeva_ota "
		voctacolumns = """id, territory2id, objectid"""
		veeva_ota = self.pg_ob.download_data(query_v_ota, self.tmp_dir, 'VeevaOT2A', voctacolumns)
		df_del = pd.merge(ota_delete, veeva_accounts, left_on=['accountnumber'], right_on=['external_id_vod'])
		df_del = pd.merge(df_del, veeva_terr2, left_on=['position_code'], right_on=['name'])
		#df_del = df_del.rename(columns={'territory2_id': 'Territory2Id', 'account_id': 'ObjectId'}, inplace=True)
		#df_del = df_del[['Territory2Id','ObjectId']]
		df_del = pd.merge(df_del, veeva_ota, left_on=['territory2_id', 'account_id'], right_on=['territory2id', 'objectid'])
		df_del = df_del[['id']]
		df_del.dropna()
		#logging.debug('----Insert File Colimns----'+df_del.columns)
		return df_del
	
	def update_success_records_dbota(self, file_path):
		count_success_rec = 0
		sucess_df = pd.read_csv(file_path, dtype='object', encoding='utf-8')
		count_success_rec = len(sucess_df)
		if count_success_rec > 0 :
			table_name_1 = 'veeva_ota'
			sc_df = sucess_df.rename(columns={'sf__Id':'id'})
			sc_df = sc_df[['id','ObjectId','Territory2Id']]
			sc_df['processeddate'] = datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4] + "Z"
			col = ['id','ObjectId','Territory2Id','processeddate']
			sc_df = self.set_column_sequence(sc_df, col)
			file_path1 = os.path.abspath(os.path.join('tmp', "ota_temp_success" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv'))
			sc_df.to_csv(file_path1,index =False)
			temptable = 'ota_temp_insert_success'
			self.pg_ob.execute_query("CREATE TABLE IF NOT EXISTS {} (id text, territory2id "
											 "text, objectid text, processeddate timestamp without time zone)".format(temptable))
			self.pg_ob.execute_query("COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(temptable, file_path1))
			self.pg_ob.execute_query(
						"DELETE FROM {} WHERE id IN (SELECT id FROM {});".format('veeva_ota', temptable))
			self.pg_ob.execute_query(
						"COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(table_name_1, file_path1))
			self.pg_ob.execute_query("TRUNCATE TABLE {}".format(temptable))
		return count_success_rec

	def insert_error_records_db(self, df):
		count_success_rec = 0
		if not df.empty:
			df.drop(columns=['sf__Id'],inplace= True)
			df['processeddate'] = datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4] + "Z"
			file_path1 = os.path.abspath(os.path.join('tmp', "ota_rejected_records" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv'))
			df.to_csv(file_path1,index = False)
			#sucess_df = pd.read_csv(file_path, dtype='object', encoding='utf-8')
			count_success_rec = len(df)
			if count_success_rec > 0 :
				table_name_1 = 'veeva_ota_rejected'
				self.pg_ob.execute_query("CREATE TABLE IF NOT EXISTS {} (sf__error text,associationcause text,objectid text,territory2id text , processeddate timestamp without time zone)".format(table_name_1))
				self.pg_ob.execute_query("COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(table_name_1, file_path1))
		return count_success_rec

	def delete_error_records_db(self, df):
		count_success_rec = 0
		if not df.empty:
			df.drop(columns=['sf__Id'],inplace= True)
			df['processeddate'] = datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4] + "Z"
			file_path1 = os.path.abspath(os.path.join('tmp', "ota_delete_rejected_records" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv'))
			df.to_csv(file_path1,index=False)
		#sucess_df = pd.read_csv(file_path, dtype='object', encoding='utf-8')
			count_success_rec = len(df)
			if count_success_rec > 0 :
				table_name_1 = 'veeva_ota_delete_rejected'
				self.pg_ob.execute_query("CREATE TABLE IF NOT EXISTS {} (sf__error text,id text, processeddate timestamp without time zone)".format(table_name_1))
				self.pg_ob.execute_query("COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(table_name_1, file_path1))
		return count_success_rec
	
	def deleted_records_dbota(self,file_path):
		count_del_rec = 0
		delete_df = pd.read_csv(file_path, dtype='object', encoding='utf-8')
		count_del_rec = len(delete_df)
		if count_del_rec > 0 :

			#er_df = delete_df.rename(columns={'sf__Id' :'id'})
			delete_df = delete_df[['id']]
			file_path2 = os.path.abspath(os.path.join('tmp', "ota_temp_deleted" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv'))
			delete_df.to_csv(file_path2,index =False)
			temptable1 = 'ota_temp_delete_success'
			self.pg_ob.execute_query("CREATE TABLE IF NOT EXISTS {} (id text)".format(temptable1))
			self.pg_ob.execute_query("COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(temptable1, file_path2))
			self.pg_ob.execute_query(
					"DELETE FROM {} WHERE id IN (SELECT id FROM {});".format('veeva_ota', temptable1))
			self.pg_ob.execute_query("TRUNCATE TABLE {}".format(temptable1))
		return count_del_rec

	def sucees_error_mail(self ,count_insert_success, count_delete_success, count_insert_error, count_delete_error,file_path_insert_error):
		try:
			logging.debug('Entered mail component |||||||')
			currentdate = datetime.now().strftime("%Y-%m-%d")
			logging.debug(currentdate)
			msg = MIMEMultipart()
			emailfrom = "azglobal_salesiq_support@Axtria.onmicrosoft.com"
			emailto = "AZ_ROW_SalesIQ_DevSupport@Axtria.com,mohit.goyal1@axtria.com,yennapusa.reddy@axtria.com,illakkiya.rangaswamy2@astrazeneca.com,balasubramanian.selvarasu1@astrazeneca.com,inchglobalsalesiqsupport@astrazeneca.com,inchrowveevadi@astrazeneca.com"
			MESSAGE_BODY = """Hi Team \nPlease Find Below Todays Veeva OT2A Jobs Status. \n\nOT2A Insert Success Record Count = {}. --> DataBase Table Name : veeva_ota. \nOT2A Delete Success Record Count = {}. --> DataBase Table Name : veeva_ota. \nOT2A Insert Rejected Record Count = {}. --> DataBase Table Name : veeva_ota_rejected . \nOT2A Delete Rejected Record Count = {}. --> DataBase Table Name : veeva_ota_delete_rejected .\n\nThank You, \nAxtria SalesIQ Team.""".format(count_insert_success, count_delete_success, count_insert_error, count_delete_error)
			body_part = MIMEText(MESSAGE_BODY, 'plain')
			msg.attach(body_part)
			msg["Subject"] = self.org_name + " | Veeva OT2A Job Status | " + currentdate
			msg["From"] = emailfrom
			msg["To"] = emailto
			context=ssl.create_default_context()
			file_paths = file_path_insert_error
			zip_name = os.path.abspath(os.path.join('tmp', "ota_temp_deleted" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.zip'))
			with ZipFile(zip_name,'w') as zip:
				zip.write(file_paths)
			filename = "ota_inset_error.zip"
			attachment = open(zip_name, "rb")
			p = MIMEBase('application', 'octet-stream')
			p.set_payload((attachment).read())
			encoders.encode_base64(p)
			p.add_header('Content-Disposition', "attachment; filename= %s" % filename)
			msg.attach(p)
			with smtplib.SMTP('smtp.office365.com', 587) as smtp:
				smtp.starttls(context=context)
				smtp.login(msg["From"], "Bay87234")
				text = msg.as_string()
				smtp.sendmail(emailfrom, emailto, text)
				smtp.quit()
			return 'success' 

		except smtplib.SMTPException as e:
			logging.exception("ERROR")
			return str(e)
		

	def __del__(self):
		logging.debug("\n\n Jobs Finished.\n\n")