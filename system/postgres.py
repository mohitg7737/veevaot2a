import os
import logging
import psycopg2

from uuid import uuid4
from pandas import read_csv, DataFrame
from .salesforce import SalesforceConnector
from psycopg2 import OperationalError


class Postgres:
    def __init__(self, connection_string: str):
        self.connection_string = connection_string
        self.create_connection()

    def create_connection(self):
        self.conn = psycopg2.connect(self.connection_string)
        self.conn.autocommit = True
        self.cur = self.conn.cursor()

    @classmethod
    def initialise_from(cls, salesforce: SalesforceConnector):
        query = """SELECT AxtriaSalesIQTM__BR_PG_Database__c, AxtriaSalesIQTM__BR_PG_Host__c, AxtriaSalesIQTM__BR_PG_Password__c, AxtriaSalesIQTM__BR_PG_Port__c, AxtriaSalesIQTM__BR_PG_Schema__c, AxtriaSalesIQTM__BR_PG_UserName__c FROM AxtriaSalesIQTM__ETL_Config__c 
            WHERE Name = 'VeevaOT2A' LIMIT 1"""
        object_name = 'AxtriaSalesIQTM__ETL_Config__c'
        credentials = salesforce.query(query, object_name)
        dbname=credentials.at[0, 'AxtriaSalesIQTM__BR_PG_Database__c']
        host=credentials.at[0, 'AxtriaSalesIQTM__BR_PG_Host__c']
        port=credentials.at[0, 'AxtriaSalesIQTM__BR_PG_Port__c']
        user=credentials.at[0, 'AxtriaSalesIQTM__BR_PG_UserName__c']
        password=credentials.at[0, 'AxtriaSalesIQTM__BR_PG_Password__c']
        
        logging.debug('etl--details--------'+credentials.at[0, 'AxtriaSalesIQTM__BR_PG_Database__c'])
        connection_str = f"dbname='{dbname}' host='{host}' port='{port}' user='{user}' password='{password}'".format(
            dbname, host, port, user, password
            )

        return cls(connection_str)

    @classmethod
    def initialise_from_2(cls, salesforce: SalesforceConnector):
        query = """SELECT AxtriaSalesIQTM__Esri_DB__c, AxtriaSalesIQTM__ESRI_Password__c, AxtriaSalesIQTM__Esri_Server_IP__c, AxtriaSalesIQTM__ESRI_UserName__c from AxtriaSalesIQTM__ETL_Config__c WHERE Name = 'VeevaOT2A' LIMIT 1"""
        object_name = 'AxtriaSalesIQTM__ETL_Config__c'
        credentials = salesforce.query(query, object_name)
        dbname=credentials.at[0, 'AxtriaSalesIQTM__Esri_DB__c']
        host=credentials.at[0, 'AxtriaSalesIQTM__Esri_Server_IP__c']
        port='5432'
        user=credentials.at[0, 'AxtriaSalesIQTM__ESRI_UserName__c']
        password=credentials.at[0, 'AxtriaSalesIQTM__ESRI_Password__c']
        connection_str = f"dbname='{dbname}' host='{host}' port='{port}' user='{user}' password='{password}'".format(
            dbname, host, port, user, password
        )

        return cls(connection_str)
    # @timer
    def execute_query(self, query):
        logging.info('\n\n %s \n' % query)
        # cur = self.conn.cursor()
        # self.cur.execute(query)
        # cur.close()
        for i in range(2):
            try:
                self.cur.execute(query)
                break
            except OperationalError as e:
                logging.error("Connection Timed out. Trying again....")
                self.create_connection()
                continue

    # @timer
    def execute_function(self, function, tablesnames, temptable):
        logging.info('\n\n %s \n' % function)
        # cur = self.conn.cursor()
        # self.cur.execute(query)
        # cur.close()
        for i in range(2):
            try:
                self.cur.callproc(function, (tablesnames, temptable))
                break
            except OperationalError as e:
                logging.error("Connection Timed out. Trying again....")
                self.create_connection()
                continue

    # @timer
    def download_data(self, query, folder='tmp', file_name=str(uuid4()), column_details=DataFrame()):
        logging.info('\n\n {} \n'.format(query))
        logging.info('\n\n {} \n'.format(column_details))
        abs_file_path = os.path.abspath(os.path.join(folder, file_name + '.csv'))
        # cur = self.conn.cursor()
        # cur.execute(query)
        # df = DataFrame(cur.fetchall())
        output_query = "Copy (" + query + ") TO '{}' WITH CSV HEADER".format(abs_file_path)
        # with open(abs_file_path, 'w') as f:
        #     cur.copy_expert(output_query, f)
        logging.info('\n\n {} \n'.format(output_query))
        self.cur.execute(output_query)
        # cur.close()
        df = read_csv(abs_file_path, dtype='object')
        # logging.debug('**nitin**'+df.head().to_string())
        logging.debug("Postgres record count %d" % len(df))
        #if not column_details.empty:
            # df.columns = column_details["Source_Column__c"].values.tolist()
            #df.columns = column_details[column_details['tb_col_nm__c'].isin(df.columns)]['Source_Column__c']
        df = df.apply(lambda x: x.fillna(""))
        # df.fillna("",inplace= True)
        os.remove(abs_file_path)
        return df

    # @timer
    def return_output(self, query: str):
        logging.info('\n\n %s \n' % query)
        # cur = self.conn.cursor()
        self.cur.execute(query)
        df = DataFrame(self.cur.fetchall())
        # cur.close()
        return df

    def _in_tuple(self, iterable):
        return str(tuple(set(iterable))).replace(',)', ')')

    def delete_records(self, table_name, key_column, key_values):
        # cur = self.conn.cursor()
        query = "DELETE FROM " + table_name + " WHERE " + key_column + " IN " + _in_tuple(key_values)
        logging.debug('\n\n %s \n' % query)
        self.cur.execute(query)
        self.cur.close()

    def upload_from_csv(self, table_name, ordered_cols, file_name, delimiter=','):
        self.execute_query(
            "COPY " + table_name + " (" + ordered_cols + ") FROM '" + file_name + "' DELIMITER '" + delimiter + "' CSV HEADER")

    def upload_across_server(self, table_name, columns, filepath):

        # logging.debug('\nInserting into ESRI\n {} \n'.format(query))
        # abs_file_path = os.path.abspath(os.path.join('{}'.format(folder), "{}".format(file_name)) + '.csv')
        # cur = self.conn.cursor()
        # cur.execute(query)
        # df = DataFrame(cur.fetchall())
        copy_sql = """COPY {} ({}) FROM STDIN WITH CSV HEADER DELIMITER ','""".format(table_name, columns)
        logging.info('\n\nCopy query : {} \n'.format(copy_sql))
        with open(filepath, 'r', encoding='UTF-8') as f:
            self.cur.copy_expert(sql=copy_sql, file=f)
        logging.debug("Data Inserted in ESRI")
        # self.cur.close()
        # df = read_csv(abs_file_path, dtype = 'object')
        # logging.debug('**nitin**'+df.head().to_string())
        # logging.debug("Postgres record count %d" % len(df))
        # if not column_details.empty:
        #     # df.columns = column_details["Source_Column__c"].values.tolist()
        #     df.columns = column_details[column_details['tb_col_nm__c'].isin(df.columns)]['Source_Column__c']
        # df = df.apply(lambda x: x.fillna(""))
        # df.fillna("",inplace= True)
        # return df

    def close(self):
        self.conn.close()
