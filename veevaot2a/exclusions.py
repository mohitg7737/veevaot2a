unmanaged_objects = []   #custom ojects not part of managed package

unmanaged_fields = {}
# unmanaged_fields["Account_Terr"] = ["Team_Name__c", "AccountNumber__c"]
unmanaged_fields["Position__c"] = ["Position_Code__c","VeevaProfile__c","VeevaRole__c"]
unmanaged_fields["Position_Geography__c"] = ["Team_Instance_ID__c"]
unmanaged_fields["Position_Account__c"] = ["Product_Code__c"]

#custom object part of managed package but some of its custom fields not a part