object_mapping = {  'Position Account'                           : 'Position_Account__c',   
                    'Position Geo'                               : 'Position_Geography__c',  
                    'Position Hierarchy'                         : 'Position__c',  
                    'callplan_Base'                              : 'Position_Account_Call_Plan__c',
                    'RosterBase'                                 : 'Position_Employee__c',             
                    'CR Team Instance Config'                    : 'CR_Team_Instance_Config__c',
                    'CR Approval Config'                         : 'CR_Approval_Config__c', 
                    'Team_Instance_Configuration'                : 'Team_Instance_Configuration__c',
                    'Team_Instance_Object_Attribute__c'          : 'Team_Instance_Object_Attribute__c',
                    'Team_Instance_Object_Attribute_Call_Plan'   : 'Team_Instance_Object_Attribute__c',
                    'Team_Instance_Object_Attribute_Detail__c'   : 'Team_Instance_Object_Attribute_Detail__c',
                    'Team_Instance_Object_Attribute_Detail_Call_Plan': 'Team_Instance_Object_Attribute_Detail__c'       
                    
                }

field_mapping = {}