import os
import glob
import json
import logging
import paramiko
import traceback
import pandas as pd
import datacompy as dcp
from salesforce_bulk import SalesforceBulk
from datetime import datetime, timezone, timedelta
import time
from system.salesforce import SalesforceConnector
from system.postgres import Postgres

upload_folder = "Ready_to_Upload"


class Jobs():
    def __init__(self, sf_username: str, sf_password: str, sf_security_token: str, sf_sandbox: bool, namespace: str, ip_addr,
                 tmp_dir, veeva_sf_username: str, veeva_sf_password: str, veeva_sf_security_token: str,
                 veeva_sf_sandbox: bool, logger_id: str, logger_object_name: str):

        self.namespace = namespace
        self.response = True
        self.ip_addr = ip_addr
        self.tmp_dir = tmp_dir
        self.logger_id = logger_id
        self.logger_object_name = logger_object_name
        self.sf_salesiq = SalesforceConnector(sf_username, sf_password, sf_security_token, namespace, sf_sandbox,
                                              tmp_dir)
        self.sf_veeva = SalesforceConnector(veeva_sf_username, veeva_sf_password, veeva_sf_security_token, '', veeva_sf_sandbox, tmp_dir)
        self.bulk = SalesforceBulk(username=sf_username, password=sf_password, security_token=sf_security_token,
                                   sandbox=sf_sandbox)
        self.pg_main = Postgres.initialise_from(self.sf_salesiq)
        self.pg_ob = Postgres.initialise_from_2(self.sf_salesiq)
        self.data_object_start_datetime = datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4] + "Z"

    def invoke(self):
        try:
            logging.info('\n\n ........... OTA Generation invoked. Beginning services !!! .............\n')
            os.mkdir(os.path.join(self.tmp_dir, upload_folder))
            last_run_date, data_object_id = self.last_success_run_date(self.logger_id, self.logger_object_name)
            all_pa_table, live_team_instance = self.fetch_ti_details()
            no_load_country = self.get_no_load_country_list()
            self.run_copy_position_account_function(all_pa_table, live_team_instance)
            self.veeva_master_sync(last_run_date, data_object_id)
            salesiq_pa = self.get_data_from_pg_master()
            veeva_ota = self.get_data_from_pg_ota(no_load_country)
            join_columns = 'accountnumber, position_code'
            object_name = 'ObjectTerritory2Association'
            ota_delete, ota_insert = self.compare_data(veeva_ota, salesiq_pa, join_columns, object_name)
            insert_df = self.prepare_insert_file(ota_insert)
            response, bulk_success_filepath = self.sf_veeva.bulk_v2_functions('insert', object_name, insert_df)
            self.update_success_records_dbota(bulk_success_filepath)
            delete_df = self.prepare_delete_file(ota_delete)
            file_path_1 = os.path.join('tmp', "ota_temp_del_df" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv')
            delete_df.to_csv(file_path_1, index = False)        
            response_del, bulk_success_deleted_filepath = self.sf_veeva.bulk_v2_functions('delete',object_name, delete_df)
            #self.mark_success_logger(self.data_object_start_datetime, self.logger_id)
            self.deleted_records_dbota(bulk_success_deleted_filepath)
            return response

        except Exception as e:
            logging.exception("ERROR")
            #self.mark_error()
            return str(e)

    def fetch_ti_details(self):
        job = self.bulk.create_query_job("AxtriaSalesIQTM__Data_Set_Rule_Map__c", contentType='CSV')
        batch = self.bulk.query(job, "select AxtriaSalesIQTM__table_name__c, "
                                     "AxtriaSalesIQTM__dataset_id__r.AxtriaSalesIQTM__Data_Set_Object_Name__c, "
                                     "AxtriaSalesIQTM__dataset_id__r.AxtriaSalesIQTM__Scenario__r"
                                     ".AxtriaSalesIQTM__Team_Instance__c, "
                                     "AxtriaSalesIQTM__dataset_id__r.AxtriaSalesIQTM__Scenario__c from "
                                     "AxtriaSalesIQTM__Data_Set_Rule_Map__c where "
                                     "AxtriaSalesIQTM__dataset_id__r.AxtriaSalesIQTM__Scenario__r"
                                     ".AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = "
                                     "'Current'  and "
                                     "AxtriaSalesIQTM__dataset_id__r.AxtriaSalesIQTM__Scenario__r"
                                     ".AxtriaSalesIQTM__Scenario_Stage__c = 'Live' and "
                                     "AxtriaSalesIQTM__dataset_id__r.AxtriaSalesIQTM__Scenario__r"
                                     ".AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r"
                                     ".AxtriaSalesIQTM__Country__r.AxtriaARSnT__Veeva_Push__c = true and "
                                     "AxtriaSalesIQTM__dataset_id__r.AxtriaSalesIQTM__Data_Set_Object_Name__c in ("
                                     "'Account','AxtriaSalesIQTM__Position_Account__c')")
        self.bulk.close_job(job)
        while not self.bulk.is_batch_done(batch):
            time.sleep(10)
        all_accounts_table = []
        all_pos_account_table = []
        all_team_instances = []
        all_scenarios_ids = []
        counter = 0
        for result in self.bulk.get_all_results_for_query_batch(batch):
            for row in result:
                if counter == 0:
                    counter = counter + 1
                    continue
                temp_str = row.decode().replace('"', '').split(',')
                t_fin_do_table = 't_fin_do'
                if temp_str[1].split('\n')[0] == 'AxtriaSalesIQTM__Position_Account__c':
                    if temp_str[0] in all_pos_account_table:
                        counter = 1
                    elif t_fin_do_table in temp_str[0]:
                        all_pos_account_table.append(temp_str[0])
                else:
                    if temp_str[0] in all_accounts_table:
                        counter = 1
                    else:
                        all_accounts_table.append(temp_str[0])
                all_team_instances.append(temp_str[2].split('\n')[0])
                all_scenarios_ids.append(temp_str[3].split('\n')[0])
        logging.info('Following are Tables of Position Account Fetched from Salesforce')
        logging.info(all_pos_account_table)
        logging.info('Fetching Tables from Master Sync Object, Copying Data to Temp Table - masterTableATL')
        return all_pos_account_table, all_team_instances

    def run_copy_position_account_function(self, tablesnames, team_instance_ids):
        temptable = 'z_team_instance'
        self.pg_main.execute_query("CREATE TABLE IF NOT EXISTS z_team_instance (scenario_id text)")
        self.pg_main.execute_query("TRUNCATE z_team_instance")
        team_instance_ids_str = ', '.join(team_instance_ids)
        self.pg_main.execute_query("INSERT INTO z_team_instance(scenario_id) VALUES ('" + team_instance_ids_str + "')")
        self.pg_main.execute_query("CREATE TABLE IF NOT EXISTS z_master_pa (accountnumber text, position_code text, "
                                   "status text)")
        self.pg_main.execute_function('posgresFunctionForMasterSyncCopy', tablesnames, temptable)
        logging.info('Data Successfully fetched from Master Sync Table')

    def veeva_master_sync(self, last_run_date, data_object_id):
        try:
            query_account = 'SELECT Id,External_ID_vod__c, Status_AZ__c,Country_vod__r.Name FROM Account WHERE ' \
                            'createddate >= {}'.format(last_run_date)
            query_territory2 = 'SELECT Id, Name FROM Territory2 WHERE LastModifiedDate >= {}'.format(last_run_date)
            veeva_delta_accounts = self.sf_veeva.bulk_download('Account', query_account,
                                                               'veeva_account_' + datetime.now().strftime(
                                                                   "%Y-%m-%d %H-%M-%S"))

            self.pg_ob.execute_query("CREATE TABLE IF NOT EXISTS veeva_account (account_id text, external_id_vod "
                                     "text, status_az text,country_vod text)")
            veeva_account_table = 'veeva_account'
            if veeva_delta_accounts != '':
                df_account = pd.read_csv(veeva_delta_accounts, dtype='object', encoding='utf-8')
            if not df_account.empty:
                self.update_data_postgres(veeva_account_table, df_account)

            veeva_delta_territory2 = self.sf_veeva.bulk_download('Territory2', query_territory2, 'territory2_' + datetime.now().strftime("%Y-%m-%d %H-%M-%S"))
            if veeva_delta_territory2 != '':
                df_terr2 = pd.read_csv(veeva_delta_territory2, dtype='object', encoding='utf-8')
            veeva_territory2_table = 'veeva_territory2'
            self.pg_ob.execute_query("CREATE TABLE IF NOT EXISTS veeva_territory2 (territory2_id text, name text)")
            os.remove(veeva_delta_accounts)
            os.remove(veeva_delta_territory2)
            if not df_terr2.empty:
                self.update_data_postgres(veeva_territory2_table, df_terr2)
            #os.remove(veeva_delta_accounts)
            #os.remove(veeva_delta_territory2)
            self.mark_success(self.data_object_start_datetime, data_object_id)
            return 'success'
        except Exception as e:
            logging.exception("ERROR")
            #self.mark_error()
            return str(e)

    def update_data_postgres(self, table_name, df):
        if table_name == 'veeva_account':
            df.rename(columns={'Id': 'account_id', 'External_ID_vod__c': 'external_id_vod', 'Status_AZ__c': 'status_az',
                               'Country_vod__r.Name': 'country_vod'}, inplace=True)
            filepath = os.path.abspath(
                os.path.join('tmp', "veeva_account_" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv'))
            df.to_csv(filepath, index=False)
            del df
            temptable = table_name + '_temp'
            self.pg_ob.execute_query("CREATE TABLE IF NOT EXISTS {} (account_id text, external_id_vod "
                                     "text, status_az text,country_vod text)".format(temptable))
            self.pg_ob.execute_query("COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(temptable, filepath))
            self.pg_ob.execute_query(
                "DELETE FROM {} WHERE account_id IN (SELECT account_id FROM {});".format(table_name, temptable))
            self.pg_ob.execute_query(
                "COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(table_name, filepath))
            self.pg_ob.execute_query("TRUNCATE TABLE {}".format(temptable))
        else:
            df.rename(columns={'Id': 'territory2_id', 'Name': 'name'}, inplace=True)
            filepath = os.path.abspath(
                os.path.join('tmp', "territory2_" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv'))
            df.to_csv(filepath, index=False)
            del df
            temptable = table_name + '_temp'
            self.pg_ob.execute_query("CREATE TABLE IF NOT EXISTS {} (territory2_id text, name text)".format(temptable))
            self.pg_ob.execute_query("COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(temptable, filepath))
            self.pg_ob.execute_query(
                "DELETE FROM {} WHERE territory2_id IN (SELECT territory2_id FROM {});".format(table_name, temptable))
            self.pg_ob.execute_query(
                "COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(table_name, filepath))
            self.pg_ob.execute_query("TRUNCATE TABLE {}".format(temptable))
        return 'Success'

    def last_success_run_date(self, record_id, object_name):
        object_id = self.sf_salesiq.query(
            "SELECT Id, AxtriaSalesIQTM__Last_Success_Sync_End_Date__c FROM AxtriaSalesIQTM__Data_Object__c WHERE Name = 'VeevaOT2A'", 'AxtriaSalesIQTM__Data_Object__c')
        if not object_id.empty:
            last_run_date = object_id.at[0, 'AxtriaSalesIQTM__Last_Success_Sync_End_Date__c']
            last_run_date = last_run_date.replace('+0000', 'Z')
            data_object_id = object_id.at[0,'Id']
            return last_run_date, data_object_id
        else:
            last_run_date = '2020-01-01'
            return last_run_date

    def get_no_load_country_list(self):
        no_load_countries = self.sf_salesiq.query(
            "SELECT AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c WHERE AxtriaARSnT__Veeva_Push__c = false", 'AxtriaSalesIQTM__Country__c' )
        return no_load_countries

    def mark_error(self, e, data_object_id):
        logging.error("Marking error in Data_Object__c and Logger object {}".format(self.logger_object_name))
        try:
            self.sf_salesiq.compound_update('Data_Object__c', data_object_id,
                                            {'Status__c': 'Error', 'Sync_Response_Detail__c': str(e)[:1999]})
        except Exception as e:
            logging.exception("Error")

    def mark_success(self, current_dtime, data_object_id):
        self.sf_salesiq.compound_update('Data_Object__c', data_object_id, {
            'Status__c': 'Success',
            'Last_Success_Sync_Start_Date__c': current_dtime,
            'Last_Success_Sync_End_Date__c': self.current_datetime()
        })

    def get_data_from_pg_master(self):
        query = "SELECT accountnumber, position_code FROM z_master_pa where status IN ('Active', 'Future Active')"
        columns = """accountnumber, position_code"""
        return self.pg_main.download_data(query, self.tmp_dir, 'MasterPASalesIQ', columns)

    def get_data_from_pg_ota(self, no_load):
        query = "SELECT objectid, territory2id FROM veeva_ota "
        columns = """objectid, territory2id"""
        df_ota = self.pg_ob.download_data(query, self.tmp_dir, 'VeevaOT2A', columns)
        query_v_account = "SELECT account_id, external_id_vod, country_vod FROM veeva_account WHERE status_az = 'Active'"
        vacolumns = """account_id, external_id_vod, country_"""
        veeva_accounts = self.pg_ob.download_data(query_v_account, self.tmp_dir, 'VeevaAcc', vacolumns)
        query_v_terr2 = "SELECT territory2_id, name FROM veeva_territory2"
        vtcolumns = """territory2_id, name """
        veeva_terr2 = self.pg_ob.download_data(query_v_terr2, self.tmp_dir, 'VeevaTerr2', vtcolumns)
        df_ota = pd.merge(df_ota, veeva_accounts, left_on=['objectid'], right_on=['account_id'])
        df_ota = pd.merge(df_ota, veeva_terr2, left_on=['territory2id'], right_on=['territory2_id'])
        df_ota = df_ota[~df_ota.country_vod.isin(no_load)]
        df_ota = df_ota.loc[:, ['external_id_vod', 'name']]
        df_ota.rename(columns={'external_id_vod': 'accountnumber', 'name': 'position_code'}, inplace=True)
        return df_ota

    def compare_data(self, df1, df2, join_columns, object_name):
        to_delete = os.path.abspath(
            os.path.join('tmp', object_name + "_delete_" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv'))
        to_insert = os.path.abspath(
            os.path.join('tmp', object_name + "insert_" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv'))
        compare = dcp.Compare(df1, df2, join_columns=['accountnumber', 'position_code'], abs_tol=0, rel_tol=0, df1_name='original',
                              df2_name='new', filepath1=to_delete, filepath2=to_insert)
        #logging.debug("Compare Results" + compare)
        df_delete = pd.read_csv(to_delete, dtype='object', encoding='utf-8')
        df_insert = pd.read_csv(to_insert, dtype='object', encoding='utf-8')
        return df_delete, df_insert

    def current_datetime(self):
        return datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4] + "Z"

    def set_column_sequence(self, dataframe, seq, front=True):
        cols = seq[:]
        for x in dataframe.columns:
            if x not in cols:
                if front:
                    cols.append(x)
                else:
                    cols.insert(0, x)
        return dataframe[cols]

    def prepare_insert_file(self, ota_insert):
        query_v_account = "SELECT account_id, external_id_vod FROM veeva_account WHERE status_az = 'Active'"
        vacolumns = """account_id, external_id_vod"""
        veeva_accounts = self.pg_ob.download_data(query_v_account, self.tmp_dir, 'VeevaAcc', vacolumns)
        query_v_terr2 = "SELECT territory2_id, name FROM veeva_territory2"
        vtcolumns = """territory2_id, name"""
        veeva_terr2 = self.pg_ob.download_data(query_v_terr2, self.tmp_dir, 'VeevaTerr2', vtcolumns)
        df_new = pd.merge(ota_insert, veeva_accounts, left_on=['accountnumber'], right_on=['external_id_vod'])
        df_new = pd.merge(df_new, veeva_terr2, left_on=['position_code'], right_on=['name'])
        df_new.rename(columns={'territory2_id': 'Territory2Id', 'account_id': 'ObjectId'}, inplace=True)
        df_new = df_new[['Territory2Id','ObjectId']]
        df_new['AssociationCause'] = 'Territory2Manual'
        df_new = df_new.dropna()
        logging.debug('----Insert File Colimns----'+df_new.columns)
        return df_new

    def prepare_delete_file(self, ota_delete):
        query_v_account = "SELECT account_id, external_id_vod FROM veeva_account WHERE status_az = 'Active'"
        vacolumns = """account_id, external_id_vod"""
        veeva_accounts = self.pg_ob.download_data(query_v_account, self.tmp_dir, 'VeevaAcc', vacolumns)
        query_v_terr2 = "SELECT territory2_id, name FROM veeva_territory2"
        vtcolumns = """territory2_id, name"""
        veeva_terr2 = self.pg_ob.download_data(query_v_terr2, self.tmp_dir, 'VeevaTerr2', vtcolumns)
        query_v_ota = "SELECT id, territory2id, objectid FROM veeva_ota "
        voctacolumns = """id, territory2id, objectid"""
        veeva_ota = self.pg_ob.download_data(query_v_ota, self.tmp_dir, 'VeevaOT2A', voctacolumns)
        df_del = pd.merge(ota_delete, veeva_accounts, left_on=['accountnumber'], right_on=['external_id_vod'])
        df_del = pd.merge(df_del, veeva_terr2, left_on=['position_code'], right_on=['name'])
        #df_del = df_del.rename(columns={'territory2_id': 'Territory2Id', 'account_id': 'ObjectId'}, inplace=True)
        #df_del = df_del[['Territory2Id','ObjectId']]
        df_del = pd.merge(df_del, veeva_ota, left_on=['territory2_id', 'account_id'], right_on=['territory2id', 'objectid'])
        df_del = df_del[['id']]
        df_del.dropna()
        #logging.debug('----Insert File Colimns----'+df_del.columns)
        return df_del
    
    def update_success_records_dbota(self, file_path):
        sucess_df = pd.read_csv(file_path, dtype='object', encoding='utf-8')
        count_success_rec = len(sucess_df)
        if count_success_rec > 0 :
                table_name_1 = 'veeva_ota'
                sc_df = sucess_df.rename(columns={'sf__Id':'id'})
                sc_df = sc_df[['id','Territory2Id','ObjectId']]
                file_path1 = os.path.abspath(os.path.join('tmp', "ota_temp_success" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv'))
                sc_df.to_csv(file_path1,index =False)
                temptable = 'ota_temp'
                self.pg_ob.execute_query("CREATE TABLE IF NOT EXISTS {} (id text, territory2id "
                                         "text, objectid text)".format(temptable))
                self.pg_ob.execute_query("COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(temptable, file_path1))
                self.pg_ob.execute_query(
                    "DELETE FROM {} WHERE id IN (SELECT id FROM {});".format('veeva_ota', temptable))
                self.pg_ob.execute_query(
                    "COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(table_name_1, file_path1))
                self.pg_ob.execute_query("TRUNCATE TABLE {}".format(temptable))
        return count_success_rec
    
    def deleted_records_dbota(self,file_path):
        delete_df = pd.read_csv(file_path, dtype='object', encoding='utf-8')
        count_del_rec = len(delete_df)
        if count_del_rec > 0 :
            #er_df = delete_df.rename(columns={'sf__Id' :'id'})
            delete_df = delete_df[['id']]
            file_path2 = os.path.abspath(os.path.join('tmp', "ota_temp_deleted" + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.csv'))
            delete_df.to_csv(file_path2,index =False)
            temptable1 = 'ota_temp1'
            self.pg_ob.execute_query("CREATE TABLE IF NOT EXISTS {} (id text)".format(temptable1))
            self.pg_ob.execute_query("COPY {} FROM '{}' DELIMITER ',' CSV HEADER".format(temptable1, file_path2))
            self.pg_ob.execute_query(
                    "DELETE FROM {} WHERE id IN (SELECT id FROM {});".format('veeva_ota', temptable1))
            self.pg_ob.execute_query("TRUNCATE TABLE {}".format(temptable1))
        return count_del_rec
    
    def __del__(self):
        logging.debug("\n\n Jobs Finished.\n\n")