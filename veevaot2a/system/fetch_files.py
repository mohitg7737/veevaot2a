import os 
import boto3
import pysftp 
import paramiko
import logging
import pdb

class SFTP:
    def __init__(self, username, password, host):
        self.cnopts          = pysftp.CnOpts()
        self.cnopts.hostkeys = None
        self.sftp            = pysftp.Connection(host, username=username, password=password, cnopts=self.cnopts)        
    
    def download(self, file_list, path, tmp_dir):
        for file_name in file_list:
            file_name += '.csv'
            # pdb.set_trace()
            if self.sftp.isfile(path + '/' + file_name): 
                logging.debug("File {} Found. Downloading ... ".format(file_name))
                self.sftp.get(remotepath=path + '/' + file_name, localpath=os.path.join(tmp_dir,file_name))
            # elif self.sftp.isfile(path + '/Config Data/' + file_name):
            #     logging.debug("File {} Found inside Config Data. Downloading ... ".format(file_name))
            #     self.sftp.get(remotepath=path + '/' + file_name, localpath=os.path.join(tmp_dir,file_name))
            # else:
            #     logging.debug("File not found at"+ path + '/' + file_name)

    def close_conn(self):
        self.sftp.close()


class S3:
    def __init__(self, aws_access_key, aws_secret_key, file_list, bucket, tmp_dir):
        self.access_key = aws_access_key
        self.secret_key = aws_secret_key
        self.file_list = file_list
        self.path = bucket
        self.tmp_dir = tmp_dir
        
    def download(self):
        s3_conn = boto3.client('s3', aws_access_key_id = self.access_key,
                              aws_secret_access_key = self.secret_key)
        for file_name in self.file_list:
            logging.debug('File {} Found. Downloading ... '.format(file_name))
            s3_conn.download_file(self.bucket, self.path, file_name)

class AWS:
    def copy(self, file_list, filepath, tmp_dir):        
        for file in os.listdir(filepath):
            if file.endswith(".csv"):
                logging.debug("Copying file {} ".format(file))
                copyfile(os.path.abspath(os.path.join(filepath,file)),
                         os.path.abspath(os.path.join(tmp_dir,"{}".format(file))))
        for file in os.listdir(os.path.abspath(os.path.join(filepath, "Config Data"))):
            logging.debug("Copying file {} ".format(file))
            copyfile(os.path.abspath(os.path.join(filepath,"Config Data",file)),
                     os.path.abspath(os.path.join(tmp_dir,file)))



            
        
        
        