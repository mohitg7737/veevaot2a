import os
import re
import json
import logging
import requests
import pandas as pd

from time import sleep
from uuid import uuid4
from datetime import datetime
from salesforce_bulk import SalesforceBulk
from requests.exceptions import ConnectionError
from multiprocessing import Process, Lock, Value
from simple_salesforce import Salesforce, exceptions

from exclusions import unmanaged_objects
from user.data_processor import append_namespace_string

session  = requests.Session()
logging.getLogger('urllib3').setLevel(logging.CRITICAL)


SF_NEW_VERSION   = "45.0" 
BULK_V2_URI      = 'jobs/ingest/'
ENCODING         = 'utf-8'
BATCH_TIMEOUT    = 120
BATCH_ATTEMPTS   = 10
BATCH_SLEEP_TIME = 2
TIMEOUT_ATTEMPTS = 3

class SalesforceConnector:
    pool = {}

    def __init__(self, username, password, security_token, namespace, sandbox, tmp_dir: str):
        self.namespace  = namespace
        self.tmp_dir    = tmp_dir
        self.sf         = Salesforce(username=username, password=password, security_token=security_token, sandbox=sandbox)
        self.bulk_sf    = SalesforceBulk(username=username, password=password, security_token=security_token, sandbox=sandbox)
        self.sf_v2      = SalesForce_V2(self.sf)

    @classmethod
    def initialise_from_pool(cls, username: str, password: str, security_token: str, namespace: str, sandbox: bool, tmp_dir: str):
        if username + password not in SalesforceConnector.pool:
            logging.debug("Registering new connection in pool for %s " % username)
            SalesforceConnector.pool[username + password] = cls(username, password, security_token, namespace, sandbox, tmp_dir)
        else:
            salesforce = SalesforceConnector.pool.get(username + password)
            try:
                logging.debug("Checking if SF connection has expired")
                salesforce.query("SELECT count() FROM Account LIMIT 1")
                logging.debug("SF connection OK!")
            except Exception as e:
                logging.error(e)
                logging.error("Salesforce connection has expired. Re-establishing and recycling the pool")
                SalesforceConnector.pool.pop(username + password)
                SalesforceConnector.pool[username + password] = cls(username, password, security_token, namespace, sandbox, tmp_dir)
        return SalesforceConnector.pool.get(username + password)

    # @timer
    def query(self, query: str, object_name, remove_namespace=False):
        query = query
        logging.info('\n\n %s \n' % query)
        for i in range(10):
            try:
                records = self.sf.query_all(query)
                break
            except ConnectionError as e:
                logging.error("Connection Timed out. Trying again....")
                continue
        return self._prepare_df(records['records'], remove_namespace)

    # @timer
    def query_all(self, query: str, object_name, remove_namespace=True) -> pd.DataFrame:
        query = append_namespace_string(self.namespace, object_name, query)
        logging.info('\n\n %s \n' % query)
        records = []
        headers = {'Authorization': 'Bearer ' + self.get_session()}
        response = requests.get(self.sf.base_url + 'queryAll/?q=' + query, headers=headers).json()
        records += response['records']
        while not response['done']:
            logging.debug("nextRecordsUrl - " + response['nextRecordsUrl'])
            response = requests.get(self.sf.base_url + 'query/' + response['nextRecordsUrl'].split('/')[-1],
                                    headers=headers).json()
            records += response['records']
        return self._prepare_df(records, remove_namespace)

    def bulk_download(self, object_name: str, query: str, filename=None, pk_chunking=240000, remove_namespace=True) -> str:
        query = append_namespace_string(self.namespace, object_name, query)
        query_filter = query.split("FROM", maxsplit=1)[1]
        count_query = "SELECT count() FROM {} LIMIT 400000".format(query_filter)
        logging.debug("Count Query :- \n\n {} \n".format(count_query))
        for i in range(5):
            try:
                count = self.sf.query(count_query)['totalSize']
                logging.debug("Count of valid records from query = {}".format(count))
                break
            except ConnectionError as e:
                    logging.error("Connection Timed out. Trying again....")
                    continue
            except Exception:
                logging.error("Count query timed out. Downloading with pk_chunking now.")
                logging.exception("ERROR")
                count = 400001
                break
        if not filename:
            filename = object_name + "_" + datetime.now().strftime("%Y-%m-%d %H-%M-%S")
        intermediate_csv_file = os.path.join(self.tmp_dir, filename + '.csv')
        if query.find('__r.') == -1 and count <= 20000:
            logging.debug("Downloading via REST API")
            df = self.query(query, object_name, remove_namespace)
            df.to_csv(intermediate_csv_file, index=False)
            return os.path.abspath(intermediate_csv_file)
            # df.to_csv(intermediate_csv_file, index=False)
            # return intermediate_csv_file
        logging.info('\n\n %s \n' % query)

        if count:
            pk_chunking = False if count < 400000 else pk_chunking
            query_job = self.bulk_sf.create_query_job(object_name=self._prepare_object_name(object_name), contentType='CSV', 
                                                      pk_chunking=pk_chunking)
            batch = self.bulk_sf.query(query_job, query)
            if not pk_chunking:
                logging.debug("Downloading Without pk_chunking")
                self.bulk_sf.close_job(query_job)
                while not self.bulk_sf.is_batch_done(batch):
                    sleep(2)
                logging.debug("Batch for %s complete." % object_name)
                with open(intermediate_csv_file, mode='a', encoding='utf-8') as f:
                    for result in self.bulk_sf.get_all_results_for_query_batch(batch):
                        for each in result:
                            for attempt in range(BATCH_ATTEMPTS):
                                try:
                                    data = each.decode().replace('""', '')
                                    f.write(data)
                                    break
                                except (requests.RequestException, exceptions.SalesforceGeneralError):
                                    logging.error("ERROR. Retrying after {} seconds".format(BATCH_SLEEP_TIME))
                                    sleep(BATCH_SLEEP_TIME)
                                    continue 
                logging.debug("Write complete.")
                if remove_namespace:        
                    df = pd.read_csv(intermediate_csv_file, dtype='object')
                    df.columns = [col.replace(self.namespace, '') for col in df.columns]
                    df.to_csv(intermediate_csv_file, index=False)
                return os.path.abspath(intermediate_csv_file)
            a = 0
            logging.debug("Waiting for batch to finish processing")
            sleep(5)
            while a <=0:
                l = []
                batch_list = self.bulk_sf.get_batch_list(query_job)
                for i in batch_list:
                    l.append(i['state'])
                # logging.debug(set(l))
                if 'NotProcessed' in l and 'InProgress' not in l and 'Queued' not in l:
                        a = 1
                if a != 1:
                    logging.debug("Waiting for batch to finish processing")
                    sleep(10)
            logging.debug("Batch for %s complete." % object_name)
            logging.debug("No of internal batches created = {}".format(len(batch_list)))
            valid_batches = []
            for i in batch_list:
                if int(i['numberRecordsProcessed']) > 0:
                    valid_batches.append(i)
            logging.debug("Length of Valid batches = {}".format(len(valid_batches)))
            lock = Lock()
            write_header = Value('i', 1)
            process_count = min(len(valid_batches), os.cpu_count()-2)
            processes = []
            start = 0
            for i in range(process_count):
                if i == process_count-1:
                    processes.append(Process(target=self.fetch_batch_data, args=(intermediate_csv_file, query_job, valid_batches[start:], 
                                                                                 lock, write_header)))
                else:
                    processes.append(Process(target=self.fetch_batch_data, args=(intermediate_csv_file, query_job, 
                                                                                 valid_batches[start:int(((i+1)*len(valid_batches))/(process_count))], 
                                                                                 lock, write_header)))
                    start = int(((i+1)*len(valid_batches))/(process_count))

            for p in processes:
                p.start()
            sleep(2)
            logging.debug("All processes started")
            for p in processes:
                p.join()
            logging.debug("All processes have finished")

            logging.debug("Write complete.")
            self.bulk_sf.close_job(query_job)
        else:
            logging.debug("Skipping querying of data as record count = 0")
            with open(intermediate_csv_file,'a') as f:
                    f.write("Records not found for this query")
            logging.debug("Empty file created")
        if remove_namespace:        
            df = pd.read_csv(intermediate_csv_file, dtype='object')
            df.columns = [col.replace(self.namespace, '') for col in df.columns]
            df.to_csv(intermediate_csv_file, index=False)
        return os.path.abspath(intermediate_csv_file)

    def fetch_batch_data(self, intermediate_csv_file, query_job, batches, lock, write_header):
        # logging.debug("Fetching batch data by process {}".format(os.getpid()))
        # logging.debug("Batch List length = {}".format(len(batches)))
        for batch in batches:
            break_loop = False
            try:
                for timeout in range(TIMEOUT_ATTEMPTS):
                    if break_loop:
                        break
                    if timeout != 0:
                        logging.debug("Trying batch after {} minutes".format(BATCH_TIMEOUT/60))
                        sleep(BATCH_TIMEOUT)
                    for attempt in range(BATCH_ATTEMPTS):
                        try:
                            # logging.debug("Header Flag while fetching: {}".format(write_header.value))
                            logging.debug("Process {} Retrieving records for batch : {} ({} of {})".format(os.getpid(), batch['id'], batches.index(batch)+1, len(batches))) if attempt == 0 else ''
                            logging.info("Reattempting batch {} for the {} time".format(batch['id'], attempt + 1)) if attempt > 0 else ''
                            result        = self.bulk_sf.get_all_results_for_query_batch(batch['id'],query_job)
                            result_byte   = b''.join(next(result))
                            result_string = result_byte.decode().replace('""','')
                            lock.acquire()
                            # logging.debug("Header Flag while writing: {}".format(write_header.value))
                            if write_header.value == 1:
                                logging.debug("Writing batch data with header")
                                write_header.value = 0
                            else:
                                result_string = result_string[result_string.find('\n')+1:]
                            lock.release()
                            lock.acquire()
                            with open(intermediate_csv_file, mode='a', encoding='utf-8') as f:
                                f.write(result_string)
                            lock.release()
                            break_loop = True
                            break
                        except (requests.RequestException, exceptions.SalesforceGeneralError):
                            logging.error("ERROR. Trying again after {} seconds".format(BATCH_SLEEP_TIME))
                            sleep(BATCH_SLEEP_TIME)
                            continue
            except Exception:
                logging.exception("ERROR")
                raise ValueError("Could not fetch batch from SF. Try again later")

    # @timer
    def bulk_upload(self, object_name: str, df: pd.DataFrame):
        try:
            df = df.drop('Id', axis=1)
        except KeyError:
            logging.debug("Id column not present in upload. Proceeding.")
        if not df.empty:
            self.bulk_v2_functions("insert", object_name, df)

    # @timer
    def bulk_delete(self, object_name: str, df):        
        if isinstance(df, pd.Series):
            logging.warning('to be deleted records found series, converting to dataframe.')
            df = df.to_frame('Id')
        if not df.empty:
            while(True):
                status = self.bulk_v2_functions("delete", object_name, df)
                if int(status["numberRecordsFailed"]) == 0:
                    logging.debug("all records deleted succesfully")
                    return ''
                failures = self.sf_v2.fetch_v2_job_failed_records(status["id"])
                filepath = os.path.join(self.tmp_dir, "failed_" + object_name + str(uuid4()) + '.csv')
                self.write_records_from_sf(failures, object_name, filepath)
                df = pd.read_csv(filepath, dtype='object')
                failure_reason = df.at[0,"sf__Error"]    
                if failure_reason.lower().find("unable_to_lock_row") != -1:
                    logging.warning("Some records failed to delete : 'unable to obtain exclusive lock'. Retrying on failed records")
                    # df.rename(columns = {"sf__Id":"Id"}, inplace=True)
                    df = df[["Id"]] 
                elif failure_reason.lower().find("cascade") != -1 and int(status["numberRecordsFailed"]) > 1: #count > 1 because otherwise no point in trying again with batch_size=1 
                    return self.cascade_delete(object_name, df)                   
                else:
                    return failure_reason
                os.remove(filepath)
        return ''
                    
    def write_records_from_sf(self, response, object_name, filepath):
        try:
            # logging.debug("Writing records from SF for {} at {}".format(object_name, filepath))
            with open(filepath, 'w', encoding=ENCODING) as f:
                f.write(response.content.decode('UTF-8')) 
        except Exception:
            logging.warning("UTF-8 encoding failed. Trying with ASCII")
            with open(filepath, 'w') as f:
                f.write(response.content.decode('ASCII',"ignore"))

    def cascade_delete(self, object_name, df):
        logging.debug("Attempting delete with batch_size = 1 to circumvent cascade_delete error")
        for i in df["Id"]:
            try:
                getattr(self.sf, object_name).delete(i)
            except Exception:
                logging.error("Cascade delete failure")
                return "Cascade delete Failure"
        return ''

    # @timer
    def bulk_update(self, object_name, df):
        if not df.empty:
            self.bulk_v2_functions("update", object_name, df)

    def bulk_v2_functions(self, operation: str, object_name: str, df: pd.DataFrame):
        df.columns = df.columns.str.replace(self.namespace, '')
        if self.namespace and object_name not in unmanaged_objects:
            logging.debug("Appending namespaces to DF columns.")
        # df.columns = [self.namespace+x if x.endswith('__c') else x for x in df.columns.tolist()]
            prepared_columns = append_namespace_string(self.namespace, object_name.replace(self.namespace, ''), ", ".join(df.columns))
            df.columns = [i.strip() for i in prepared_columns.split(',')]
        object_name = self._prepare_object_name(object_name)
        filepath = os.path.abspath(os.path.join(self.tmp_dir, object_name+str(uuid4())+ ".csv"))
        # logging.debug(filepath)
        df.to_csv(filepath, index=False)
        fil = filepath
        FILESIZE = os.stat(filepath).st_size//(1024*1024)
        no_of_files = 1
        if FILESIZE > 100: #salesorce limitation. Max recomended 100 MB (after their encodig, increases to 150 MB)
            no_of_files = int(FILESIZE//100) + 1
        # df = pd.read_csv(filepath, dtype='object')
        # logging.debug("No of records = {}".format(len(df)))
        size = len(df)//no_of_files
        for i in range(no_of_files):
            job_id = self.sf_v2.create_v2_job(operation, object_name)
            # logging.debug("Job_id: {}".format(job_id))
            filepath = filepath[:filepath.find(".csv")].replace("___{}".format(i-1),"") + "___"+str(i)+filepath[filepath.find(".csv"):]
            if i == no_of_files - 1:
                df.iloc[i*size:].to_csv(filepath, index=False)
            else: 
                df.iloc[i*size:(i+1)*size].to_csv(filepath, index=False)
            try:
                upload_response = self.sf_v2.upload_v2(job_id, filepath)
                if upload_response.status_code != 201:
                    raise requests.HTTPError("Malformed Request. HTTPError{}".format(upload_response.status_code))
                patch_response = self.sf_v2.patch_v2_job(job_id,"UploadComplete")
                if isinstance(patch_response,list):
                    logging.error("Patch Failed")
                    logging.debug(patch_response)
                    os.remove(filepath)
                    raise ValueError
                self.sf_v2.wait_for_v2_job(job_id)
                status = self.sf_v2.get_v2_job_status(job_id)
                logging.debug(status)
                if int(status['numberRecordsFailed']) > 0:               
                    logging.warning("No of records failed : {}".format(status['numberRecordsFailed']))
                    if operation.lower() == 'insert':
                        retries = 10
                        while(retries):                            
                            r = self.sf_v2.fetch_v2_job_failed_records(job_id)
                            with open(os.path.join(self.tmp_dir, "failed_records_{}.csv".format(object_name)), 'w+') as f:
                                f.write(r.text)
                            #error_filepath = os.path.join(self.tmp_dir, "failed_records_{}.csv".format(object_name))    
                            failed_records = pd.read_csv(os.path.join(self.tmp_dir, "failed_records_{}.csv".format(object_name)))
                            error_df = failed_records[~failed_records["sf__Error"].lower().str.contains("unable_to_lock_row", na=False)]
                            logging.debug(error_df.head())
                            count_failed_rec = len(failed_records)
                            if count_failed_rec != int(status['numberRecordsFailed']):
                                logging.warning("Salesforce did not send all failed records. Retrying !!!")
                                retries -=1
                                continue
                            else:
                                logging.debug("All failed records fetched from Salesorce.")
                                break
                        if failed_records.at[0,'sf__Error'].lower().find('unable_to_lock_row') != -1:
                            logging.warning("unable_to_lock_row error during insert. Trying again for failed records")
                            # os.remove(filepath)
                            self.bulk_v2_functions('insert', object_name, failed_records.drop(columns=['sf__Id', 'sf__Error']))
                sp = self.sf_v2.fetch_v2_job_success_records(job_id)
                with open(os.path.join(self.tmp_dir, "success_records_{}.csv".format(object_name)), 'w+') as f:
                    f.write(sp.text)      
                success_filepath = os.path.join(self.tmp_dir, "success_records_{}.csv".format(object_name))
                #success_records = pd.read_csv(os.path.join(self.tmp_dir, "success_records_{}.csv".format(object_name)))
                #count_success_rec = len(success_records)
                #count_failed_rec = len(failed_records)
                os.remove(filepath)
            except Exception as e:
                patch_response = self.sf_v2.patch_v2_job(job_id, "Aborted")
                logging.error(e)
                os.remove(filepath)
                os.remove(fil)
                raise e
        logging.debug("\n\n {} complete\n".format(operation))
        os.remove(fil)
        return success_filepath, error_df

    def update(self, object_name, object_id, key, value):
        logging.debug(" %r %r " % (key, value))
        getattr(self.sf, self._prepare_object_name(object_name)).update(object_id, {self.namespace + key: value})

    def compound_update(self, object_name, object_id, values):
        values = self._prepare_keys(values)
        logging.debug(values)
        getattr(self.sf, self._prepare_object_name(object_name)).update(object_id, values)

    def get_session(self):
        return self.sf.session_id

    def get_apex_url(self):
        return self.sf.apex_url

    def get_pg_col_name(self, sfdc_col_name, column_details):
        return column_details.loc[
            (column_details['Source_Column__c'] == self._prepare_object_name(sfdc_col_name)),
            'tb_col_nm__c'].reset_index().loc[
            0, 'tb_col_nm__c']

    def update_column(self, df, column_name, column_value):
        df[self._prepare_object_name(column_name)] = column_value
        return df

    def _prepare_query(self, query: str) -> str:
        query = query.replace(self.namespace, "")
        for custom_object in re.findall('( [0-9aA-zZ_]+__[cC]| [0-9aA-zZ_]+__[rR])', query):
            query = query.replace(custom_object, ' ' + self._prepare_object_name(custom_object[1:]))
        for custom_object in re.findall('(\.[0-9aA-zZ_]+__[cC])', query):
            query = query.replace(custom_object, '.' + self._prepare_object_name(custom_object[1:]))
        for custom_object in re.findall('(\.[0-9aA-zZ_]+__[rR])', query):
            query = query.replace(custom_object, '.' + self._prepare_object_name(custom_object[1:]))
        return query

    def _prepare_df(self, records, remove_namespace=True):
        df = pd.DataFrame(records)
        try:
            if remove_namespace:
                df.columns = df.columns.str.replace(self.namespace, "")
            df = df.drop('attributes', axis=1)
        except (KeyError, AttributeError):
            pass
            # logging.warning(e)
            # logging.debug("attributes column not found or DF is empty. Proceeding.")
        logging.info("No. of records fetched %d" % len(df))
        return df

    def _prepare_keys(self, values):
        result = {}
        for key, value in values.items():
            result[self.namespace + key] = value
        return result

    def _prepare_object_name(self, object_name):
        if object_name.find(self.namespace) == -1 and (object_name.endswith('__c') or (object_name.endswith('__C')) or object_name.endswith('__r')):
            return self.namespace + object_name
        return object_name


class SalesForce_V2:
    def __init__(self, sf):
        self.sf = sf
        self.uploaded = False
        self.RecordsProcessed = None
        self.RecordsFailed = None
        self.wait_time = 0
        self.base_url = self.new_base_url()
        
    def new_base_url(self):
        return self.sf.base_url.replace(self.sf.sf_version, SF_NEW_VERSION)

    def create_v2_job(self, operation: str, object_name: str):
        headers     = {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        body        = {
                        "object" : object_name,
                        "contentType" : "CSV",
                        "operation" : operation
                      }
        for i in range(5):
            try:
                result      = session.request("POST", self.base_url + BULK_V2_URI, headers=headers, 
                                              data=json.dumps(body))
                break
            except ConnectionError as e:
                logging.error("Connection Timed out. Trying again....")
                continue  
                
        response    = result.json()
        job_id      = response["id"]
        return job_id

    
    def upload_v2(self, job_id: str, CSV_filepath: str):
        # logging.debug("Uploading CSV to SFDC ...")
        headers     = {
                        'Content-Type': 'text/csv',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        body        = open(CSV_filepath, 'r').read()
        self.wait_time = max(1,len(pd.read_csv(CSV_filepath))/8000)
        result      = session.request("PUT", "{}/batches".format(self.base_url + BULK_V2_URI + job_id),
                                      headers=headers, data=body)
        self.uploaded = True
        return result
    
    def get_v2_job_status(self, job_id: str):
        headers     = {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        result      = session.request("GET", "{}/".format(self.base_url + BULK_V2_URI + job_id), headers=headers)
        response    = result.json()
        # logging.debug(response['state'])
        return response
    
    def patch_v2_job(self, job_id: str, patch_type: str):
        # logging.debug("Pushing Job patch. {}".format(patch_type))
        if patch_type == "UploadComplete" and not self.uploaded:
            logging.debug("Upload CSV first")
            raise ValueError
        headers     = {
                        'Content-Type': 'application/json; charset=UTF-8',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        body        = {
                        'state':patch_type
                      }
        result      = session.request("PATCH", "{}/".format(self.base_url + BULK_V2_URI + job_id),
                                      headers=headers, data=json.dumps(body))
        response    = result.json()
        return response
    
    def wait_for_v2_job(self, job_id: str):
        logging.debug("Waiting for SFDC to process records")
        flag = True
        while flag:
            state = self.get_v2_job_status(job_id)["state"]
            if (state != 'JobComplete') and (state != 'Failed'):
                sleep((self.wait_time/2) + 0.25)
            else:
                flag = False
        return
    
    def fetch_v2_job_failed_records(self, job_id):
        logging.debug("Fetching Failed Records")
        headers     = {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        result      = session.request("GET", "{}/failedResults".format(self.base_url + BULK_V2_URI + job_id),
                                      headers=headers)
        return result

    def fetch_v2_job_success_records(self, job_id):

        logging.debug("Fetching Success Records")
        headers     = {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        result      = session.request("GET", "{}/successfulResults".format(self.base_url + BULK_V2_URI + job_id), headers=headers)

        return result

    def fetch_v2_job_unprocessed(self, job_id):
        logging.debug("Fetching Unprocessed Records")
        headers     = {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        result      = session.request("GET", "{}/unprocessedrecords".format(self.base_url + BULK_V2_URI + job_id),
                                      headers=headers)
        return result

    # def bulk_download(self, object_name: str, query: str, filename=None) -> str:
    #     query = self._prepare_query(query)
    #     logging.info('\n\n %s \n' % query)
    #     filter_clause = query.split("FROM", maxsplit=1)[1]
    #     count_query = "SELECT count() FROM {} LIMIT 300000".format(filter_clause)
    #     logging.debug("Count Query :- \n\n {} \n".format(count_query))
    #     object_name = object_name.replace(self.namespace, "")
    #     if object_name not in unmanaged_objects:
    #         object_name = self._prepare_object_name(object_name)
    #     try:
    #         record_count = self.sf.query(count_query)["totalSize"]
    #         logging.debug("Count of Valid records = {}".format(record_count))            
    #     except SfExceptions.SalesforceMalformedRequest as e:
    #         logging.error(e)
    #         logging.error("Count query timed out. Downloading with pk_chunking = True")
    #         record_count = 300001
    #     if not filename:
    #         filename = object_name + str(uuid4())
    #     intermediate_csv_file = os.path.join(self.tmp_dir, filename + '.csv')
    #     if query.find('__r') == -1 and record_count < 20000:
    #         logging.debug("Using REST API to fetch data.")
    #         df = self.query(query)
    #         df.to_csv(intermediate_csv_file, index=False)
    #         return os.path.abspath(intermediate_csv_file)
    #     if record_count:
    #         if record_count >= 300000:
    #             logging.debug("Records Count > 300000. Running with pk_chunking enabled")
    #             query_job = self.bulk.create_query_job(object_name=object_name, contentType='CSV', pk_chunking=250000)
    #             batch = self.bulk.query(query_job, query)
    #             logging.debug("Waiting for batch to finish processing")
    #             sleep(2)
    #             while True:
    #                 batch_states = []
    #                 batch_list = self.bulk.get_batch_list(query_job) #to get updated batch states every time
    #                 for batch in batch_list:
    #                     batch_states.append(batch['state'])
    #                 if not('NotProcessed' in batch_states and ('InProgress' and 'Queued') not in batch_states):
    #                     logging.debug("Waiting for batches to finish processing")
    #                     sleep(5)
    #                 else:
    #                     logging.debug("Batch for %s complete." % object_name)
    #                     logging.debug("No of internal batches created = %s " % len(batch_list))
    #                     break
    #             valid_batches = []
    #             for batch in batch_list:
    #                 if int(batch['numberRecordsProcessed']) > 0:
    #                     valid_batches.append(batch)
    #             logging.debug("Length of Valid batches = %s " % len(valid_batches))
    #             if valid_batches:
    #                 with open(intermediate_csv_file, mode='a', encoding=ENCODING) as f:
    #                     for batch in valid_batches:
    #                         logging.debug("Retrieving records for batch : %s " % batch['id'])
    #                         result        = self.bulk.get_all_results_for_query_batch(batch['id'], query_job)
    #                         result_byte   = b''.join(next(result))
    #                         result_string = result_byte.decode().replace('""','')
    #                         f.write(result_string)
    #                 df = pd.read_csv(intermediate_csv_file, header=None, encoding=ENCODING, dtype='object')
    #                 df.drop_duplicates(subset=df.columns.tolist(), keep='first', inplace=True)
    #                 df.columns = df.iloc[0]
    #                 df.drop(df.index[0], inplace=True)
    #                 df.to_csv(intermediate_csv_file, index=False, encoding=ENCODING)
    #             else:
    #                 with open(intermediate_csv_file,'a') as f:
    #                     f.write("Records not found for this query")
    #                 logging.debug("Created empty file")
    #             logging.debug("Write complete.")
    #             self.bulk.close_job(query_job)
    #         else:
    #             job = self.bulk.create_query_job(object_name, contentType='CSV')
    #             batch = self.bulk.query(job, query)
    #             while not self.bulk.is_batch_done(batch):
    #                 logging.debug("Waiting for batch to finish processing")
    #                 sleep(5)
    #             with open(intermediate_csv_file, mode='a', encoding = ENCODING) as f:
    #                 result        = self.bulk.get_all_results_for_query_batch(batch)
    #                 result_byte   = b''.join(next(result))
    #                 result_string = result_byte.decode().replace('""','')
    #                 f.write(result_string)
    #             logging.debug("Write complete.")
    #             self.bulk.close_job(job)
    #     else:
    #         logging.debug("Skipping Querying of data. Record count = 0")
    #         with open(intermediate_csv_file,'a') as f:
    #                     f.write("Records not found for this query")
    #         logging.debug("Created empty file")
    #     return os.path.abspath(intermediate_csv_file)